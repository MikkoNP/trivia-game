import VueRouter from 'vue-router'
import Vue from "vue"
import Userinfo from '@/components/Userinfo'
import Questions from '@/components/Questions'
import Result from '@/components/Result'

Vue.use(VueRouter)

const routes = [
    {
        path: '/start',
        alias: '/',
        component: Userinfo
    },
    {
        path: '/questions',
        component: Questions
    },
    {
        path: '/result',
        component: Result
    }
]

export default new VueRouter({
    routes
})
